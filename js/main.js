/**
 * @author AlexanderC
 */

(function() {
  if (document.getElementsByClassName) {
      getElementsByClass = function (classList, node) {
          return (node || document).getElementsByClassName(classList);
      }
  } else {
      getElementsByClass = function (classList, node) {
          var node = node || document,
              list = node.getElementsByTagName('*'),
              length = list.length,
              classArray = classList.split(/\s+/),
              classes = classArray.length,
              result = [], i, j;

          for (i = 0; i < length; i++) {
              for (j = 0; j < classes; j++) {
                  if (list[i].className.search('\\b' + classArray[j] + '\\b') !== -1) {
                      result.push(list[i]);
                      break;
                  }
              }
          }

          return result;
      }
  }

  function fillRating() {
      var ratings = getElementsByClass('rating');

      if (ratings) {
          var star_full = "<span >★</span>";
          var star = "<span>☆</span>";

          for (var i = 0; i < ratings.length; i++) {
              var rating = ratings[i];

              var stars = parseInt(rating.getAttribute('data-stars'));
              stars = stars <= 5 ? stars : 5;

              if (stars > 0) {
                  var starsPad = 5 - stars;
                  var content = '';

                  for (var j = 0; j < stars; j++) {
                      content += star_full;
                  }

                  for (var k = 0; k < starsPad; k++) {
                      content += star;
                  }

                  rating.innerHTML = content;
              }
          }
      }
  }

  function setCurDate() {
      var date = new Date();
      var dateString = date.getDate() +
          " " +
          [
              'Jan', 'Feb', 'Mar',
              'Apr', 'May', 'Jun',
              'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'
          ][date.getUTCMonth()] +
          " " +
          date.getUTCFullYear();

      var els = getElementsByClass('cur-time');

      if (els) {
          for (var i in els) {
              if (typeof els[i] === 'object') {
                  els[i].innerHTML = dateString;
              }
          }
      }
  }

  fillRating();
  setCurDate();
})();
